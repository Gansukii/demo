import { interTypes } from "../tests";
import { InferGetStaticPropsType } from "next";
import { getAllProducts } from "@framework/product";
import { getConfig } from "@framework/api/config";
import { Layout } from "@components/common";
import { ProductCard } from "@components/product";
import { Grid, Hero, Marquee } from "@components/ui";

export async function getStaticProps() {
  const config = getConfig();
  const products = await getAllProducts(config);

  return {
    props: {
      products,
    },
    revalidate: 4 * 60 * 60,
  };
}

export default function Home({ products }: InferGetStaticPropsType<typeof getStaticProps>) {
  // useEffect(() => {
  //   interTypes();
  // }, []);

  return (
    <>
      <Grid>
        {products.slice(0, 3).map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </Grid>
      <Hero
        headline="Candy caramels sweet jelly-o fruitcake."
        description="Macaroon cupcake lollipop cake gingerbread gingerbread dragée. Powder macaroon tart croissant danish bonbon sugar plum. Liquorice macaroon jelly-o bear claw ice cream jujubes. Danish fruitcake bonbon icing toffee candy oat cake biscuit. Brownie fruitcake jelly-o jelly sesame snaps bear claw cheesecake chocolate bar oat cake. Wafer chocolate tiramisu gingerbread chocolate bar pastry shortbread cookie apple pie. Carrot cake lemon drops pastry chupa chups sweet muffin dragée wafer tootsie roll. Powder oat cake bear claw brownie gummies shortbread ice cream cotton candy. "
      />
      <Marquee>
        {products.slice(0, 3).map((product) => (
          <ProductCard key={product.id} variant="slim" product={product} />
        ))}
      </Marquee>
      <Grid layout="B">
        {products.slice(0, 3).map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </Grid>
      <Marquee variant="secondary">
        {products.slice(0, 3).map((product) => (
          <ProductCard key={product.id} variant="slim" product={product} />
        ))}
      </Marquee>
    </>
  );
}

Home.Layout = Layout;
