import { AppProps } from "next/app";
import { FC, ReactNode } from "react";
import "keen-slider/keen-slider.min.css";
import "@assets/main.css";
import { UIProvider, useUI } from "@components/ui/context";

const Noop: FC<{ children: ReactNode }> = ({ children }) => <>{children}</>;

function MyApp({ Component, pageProps }: AppProps & { Component: { Layout: FC<{ children: ReactNode }> } }) {
  const Layout = Component.Layout ?? Noop;
  const ui = useUI();

  return (
    <UIProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </UIProvider>
  );
}

export default MyApp;
