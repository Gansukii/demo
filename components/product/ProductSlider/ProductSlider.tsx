import React, { FC, Children, isValidElement, ReactNode, useState } from "react";
import styles from "./ProductSlider.module.css";
import { useKeenSlider } from "keen-slider/react";
import cn from "classnames";

const ProductSlider: FC<{ children: ReactNode }> = ({ children }) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const [loaded, setLoaded] = useState(false);

  const [sliderRef, instanceRef] = useKeenSlider({
    initial: 0,
    loop: true,
    slideChanged(slider) {
      setCurrentSlide(slider.track.details.rel);
    },
    created() {
      setLoaded(true);
    },
  });

  return (
    <div className={styles.root}>
      <div ref={sliderRef as any} className="keen-slider h-full transition-opacity">
        <button onClick={instanceRef.current?.prev} className={cn(styles.leftControl, styles.control)} />
        <button onClick={instanceRef.current?.next} className={cn(styles.rightControl, styles.control)} />
        {Children.map(children, (child) => {
          if (isValidElement(child)) {
            return {
              ...child,
              props: {
                ...child.props,
                className: `${child.props.className ? `${child.props.className}` : ""} keen-slider__slide`,
              },
            };

            // return React.cloneElement(child, {
            //   className: `${
            //     child.props.className ? `${child.props.className}` : ""
            //   } keen-slider__slide`
            // })
          }

          return child;
        })}
      </div>
    </div>
  );
};

export default ProductSlider;
