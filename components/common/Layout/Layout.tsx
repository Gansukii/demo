import { FC, ReactNode } from "react";
import style from "./Layout.module.css";
import { Footer, Navbar } from "@components/common";
import { Sidebar } from "@components/ui";
import { CartSidebar } from "@components/cart";
import { useUI } from "@components/ui/context";
import { ApiProvider } from "@framework";

const Layout: FC<{ children: ReactNode }> = ({ children }) => {
  const { isSidebarOpen, closeSidebar } = useUI();

  return (
    <>
      <div className="styles.root">
        <ApiProvider>
          <Navbar />
          <Sidebar onClose={closeSidebar} isOpen={isSidebarOpen}>
            <CartSidebar />
          </Sidebar>
          <main className={style.root}>{children}</main>
          <Footer />
        </ApiProvider>
      </div>
    </>
  );
};

export default Layout;
