export function interTypes() {
  /****** 
    Use of interface
  ******/

  // interface Person {
  //   name: string;
  //   age: number;
  // }

  /****** 
    Use of type
  ******/

  // type something = string;

  const functionTypes = () => {
    type PersonName = (firstName: string, lastName: string, age?: number) => string;

    const printName: PersonName = (fName: string, lName: string, age: number = 0): string => {
      const name = fName + " " + lName + ", " + age + " years old";

      return name;
    };

    console.log(printName("Ragan", "Lamoc", 22));
  };
  // ############ class types ###############

  const classTypes = () => {
    // OPTIONAL VALUE
    class Student {
      name: string;
      yearLevel: string;
      age?: number;

      constructor(name: string, yearLevel: string, age?: number) {
        this.name = name;
        this.yearLevel = yearLevel;
        this.age = age;
      }
    }

    function getStudent(student: Student) {
      let studentInfo = "Name: " + student.name;
      studentInfo += student.age ? student.age : "";
      return studentInfo;
    }

    /* 2 ways to declare  */
    //   const student1: Student = { name: "Ragan Lamoc", yearLevel: "4th yr" };
    const student1 = new Student("Ragan Lamoc", "4th yr");

    console.log(getStudent(student1));
  };

  const extend = () => {
    interface Student {
      name: string;
      year: string;
      headIT?: string;
      headCS?: string;
    }
    interface studIT extends Student {
      headIT: string;
    }
    interface studCS extends Student {
      headCS: string;
    }

    /* Using Type */
    // type Student = {
    //   name: string;
    //   year: string;
    // };
    // type IT = {
    //   headIT: string;
    // } & Student;
    // type CS = {
    //   headCS: string;
    // } & Student;

    const student1: studIT = { name: "John Doe", year: "4th", headIT: "someone from IT" };
    const student2: studCS = { name: "John Doe", year: "4th", headCS: "someone from CS" };
    function getStudent(student: Student) {
      let studentInfo = "Name: " + student.name + " |Year: " + student.year + " |Head: ";
      studentInfo += student.headIT ? student.headIT : student.headCS;
      return studentInfo;
    }

    return (
      <>
        <div>{getStudent(student1)} </div>

        <div>{getStudent(student2)}</div>
      </>
    );
  };

  const unionsTypeCast = () => {
    type Dog = {
      name: string;
      sound: "arf";
      biteDamage: number;
    };
    type Cat = {
      name: string;
      sound: "meow";
      clawDamage: number;
    };

    /*      declare Animal as dog or cat (union)*/
    type Animal = Dog | Cat;

    const animal: Dog = {
      name: "Brownie",
      sound: "arf",
      biteDamage: 50,
    };

    function animalUnion(animal: Animal) {
      /*      using typecast, we can force it to access the dog's biteDamage  */
      console.log((animal as Dog).biteDamage);
      /*     only animal.name and animal.sound can be accessed in the union since they both have name and sound */
      console.log(animal.name, animal.sound);
      switch (animal.sound) {
        case "arf":
          console.log("Dog's bite damage:" + animal.biteDamage);
          break;
        default:
          /*     if it is not arf, it's meow so it can also be in default  */
          console.log("cat's claw damage:" + animal.clawDamage);
          break;
      }
    }

    animalUnion(animal);
  };

  const objectTypes = () => {
    /*    generic
          | can be used as "or"
          eg. function something(someObject: { [key: string]: string | number | boolean }) {...}
          unknown can be used to get a warning if error will occur
          eg. person.age.toUpperCase() will result in error if number type was passed
    */
    function something(someObject: { [key: string]: any }) {
      console.log("this function accepts any type of value paired with its key of type string");
      // console.log(someObject.toUpperCase());
    }

    something({
      name: "John Doe",
      isLegalAge: false,
      age: 44,
    });
  };

  const arrays = () => {
    /*      creating custom generic type */
    /*      N here represents any type. In this case, number type was passed --> new Loop<number>()  */
    class Loop<N> {
      startLoop(items: N[]) {
        items.forEach((item) => {
          console.log(item);
        });
      }
    }

    /*      This will run the loop with array of numbers  */
    const loop = new Loop<number>();
    const arr = [1, 2, 3, 4, 5];

    /*      This will call the same function from the same class, but passing an array of string instead */
    // const loop = new Loop<string>();
    // const arr = ["this", "is", "an", "array"];

    loop.startLoop(arr);

    /*      Generic extends - using a class that implements an interface can be used in an array  */

    interface Person {
      name: string;
      age: number;
    }

    /*      Every property in the Person interface should be present in this class as it is implementing it 
            Removing the property name will result in error    */

    class Student implements Person {
      name = "";
      age = 0;
      hobby = "";
    }

    /*  T extends Person = Person> means that the default extend is Person */
    class Eldest<T extends Person = Person> {
      /*      This function will just return the eldest  */
      startLoop(items: T[], callback: (i: T) => void) {
        let max = items[0];
        console.log(items[0].age);
        items.forEach((item) => {
          callback(item);
          max = item.age > max.age ? item : max;
        });

        console.log(max.name + " is the eldest with the age of " + max.age);
      }
    }

    const eldest = new Eldest<Student>();

    const students = [
      { name: "John Doe", age: 4, hobby: "playing" },
      { name: "Spongebob Squaredypants", age: 9, hobby: "jellyfishing" },
      { name: "Ben Ten", age: 10, hobby: "transforming" },
      { name: "Patrick Star", age: 6, hobby: "jellyfishing" },
    ];

    eldest.startLoop(students, (item) =>
      console.log("Student " + item.name + " is " + item.age + " years old and likes " + item.hobby)
    );

    // const arr = [2, 54, 34, 44, 32, 13, 4];
  };

  const arrayTypes = () => {
    /*      For arrays. Index is a number for accessing the array elements  */
    type CustomArray = {
      [index: number]: string;
    };
    /*      For objects. key is a string for accessing the values  */
    type CustomObj = {
      [key: string]: any;
    };

    /*      Using typeof  */
    function someFunction(a: string) {
      return "Hello World";
    }

    /*      anotherFunction is a function that is the same type of someFunction (same parameters return value)  */
    const anotherFunction: typeof someFunction = (name) => "Hello " + name;

    console.log(anotherFunction("My Name"));

    /*      Multiple generic params  */
    // type FunctionType<P1 = string, P2 = number, R = string> = (param1: P1, param2: P2) => R;

    // const legitFunction: FunctionType = (name, age) => {
    //   return name + age;
    // };
    // const name = "My Name";
    // const age = 30;
    // console.log(legitFunction(name, age));
  };

  functionTypes();
  //   classTypes();
  // extend();
  // unionsTypeCast();
  // objectTypes();
  // arrays();
  // arrayTypes();
}
